package com.atguigu.jxc.controller;/*
 * @author: XueYouPeng
 * @time: 23.8.7 下午 7:21
 */


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListController {

    @Autowired
    private DamageListService damageListService;



    /*
     * 保存报损单
     * @author: XueYouPeng
     * @time: 23.8.8 上午 8:06
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public ServiceVO save(DamageList damageList , String damageListGoodsStr, HttpSession session){

        return damageListService.save(damageList, damageListGoodsStr,session);
    }

    /*
     * 报损单查询
     * @author: XueYouPeng
     * @time: 23.8.8 上午 10:08
     */
    @PostMapping(value = "/list")
    public Map<String , Object> list(String sTime , String eTime, HttpSession session){
        return damageListService.list(sTime , eTime,session);
    }

    /*
     * 查询报损单商品信息
     * @author: XueYouPeng
     * @time: 23.8.8 上午 10:34
     */
    @PostMapping(value = "/goodsList")
    public Map<String , Object> goodsList(Integer damageListId){
        return damageListService.goodsList(damageListId);
    }
}
