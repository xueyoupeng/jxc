package com.atguigu.jxc.service;/*
 * @author: XueYouPeng
 * @time: 23.8.8 上午 9:01
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverflowListGoodsService {
    ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession session);

    Map<String, Object> list(String sTime, String eTime, HttpSession session);

    Map<String, Object> goodsList(Integer overflowListId);
}
