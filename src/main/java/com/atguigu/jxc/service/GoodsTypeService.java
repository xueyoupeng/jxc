package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    ServiceVO save(String goodsTypeName, Integer pId , Integer goodsTypeState);

    ServiceVO delete(Integer goodsTypeId);

}
