package com.atguigu.jxc.service;/*
 * @author: XueYouPeng
 * @time: 23.8.5 下午 10:34
 */

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    public abstract Map<String, Object> listSupplierPage(Integer page, Integer rows, String supplierName);

    ServiceVO saveOrUpdate(Supplier supplier);

    ServiceVO delete(String ids);
}
