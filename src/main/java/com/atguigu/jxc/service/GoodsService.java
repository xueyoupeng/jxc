package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    public abstract Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    Map<String, Object> goodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    ServiceVO saveOrUpdate(Goods goods, Integer goodsTypeId );

    ServiceVO deleteGoods(Integer goodsId);

    Map<String, Object> noInventoryList(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> haveInventoryList(Integer page, Integer rows, String nameOrCode);

    ServiceVO saveorUpdateStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    ServiceVO deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();
}
