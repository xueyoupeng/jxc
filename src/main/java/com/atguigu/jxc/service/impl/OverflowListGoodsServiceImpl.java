package com.atguigu.jxc.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.8.8 上午 9:02
 */

import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {

        //先拿到用户的id
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        //保存第一部分数据
        overflowListGoodsDao.saveOverFlowList(overflowList);

        //拿到前端传过来的这个商品报溢单号
        String overflowNumber = overflowList.getOverflowNumber();
        log.info("商品报溢单号:{}" + overflowNumber);
        //用商品报溢单号查询这条数据
        OverflowList overflowListValue = overflowListGoodsDao.findOverFlow(overflowNumber);
        log.info("商品报溢数据:{}" + overflowListValue);
        //overflowListId
        Integer overflowListId = overflowListValue.getOverflowListId();
        log.info("商品报溢id:{}" + overflowListId);

        //前端传过来的JSON字符串解析成对象
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr , new TypeToken<List<OverflowListGoods>>(){}.getType());

        //补全overflowListId
        overflowListGoodsList.stream().forEach(overflowListGoods -> {
            overflowListGoods.setOverflowListId(overflowListId);
        });

        overflowListGoodsDao.saveOverFlowListValue(overflowListGoodsList);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime, HttpSession session) {

        Map<String, Object> map = new HashMap<>();

        //查询报损单
        List<OverflowList> overflowListList = overflowListGoodsDao.findList(sTime , eTime);

        //拿到用户的name
        User user = (User) session.getAttribute("currentUser");
        String trueName = user.getUserName();

        //补全name字段
        overflowListList.stream().forEach(overflowList -> {
            overflowList.setTrueName(trueName);
        });

        //封装数据
        map.put("rows" , overflowListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {

        Map<String, Object> map = new HashMap<>();

        //查询报损单商品数据
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.findGoodsList(overflowListId);

        //封装数据
        map.put("rows" , overflowListGoodsList);

        return map;
    }
}




















