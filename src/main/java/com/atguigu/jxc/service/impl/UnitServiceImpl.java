package com.atguigu.jxc.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.8.7 上午 10:07
 */

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitDao unitDao;


    @Override
    public Map<String, Object> findUnitList() {

        Map<String, Object> map = new HashMap<>();

        //查询商品单位
        List<Unit> unitList = unitDao.findUnit();

        //封装数据
        map.put("rows" , unitList);

        return map;
    }
}
