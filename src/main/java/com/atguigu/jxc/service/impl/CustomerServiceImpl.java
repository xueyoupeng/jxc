package com.atguigu.jxc.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.8.7 上午 8:22
 */

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> listCustomerPage(Integer page, Integer rows, String customerName) {

        Map<String, Object> map = new HashMap<>();

        int start = (page - 1) * rows;

        //查询客户的数据
        List<Customer> customerList = customerDao.findCustomerByName(start , rows ,customerName);

        //拿到total数据
        int total = customerList.size();

        //封装数据
        map.put("total" , total);
        map.put("rows" , customerList);

        return map;
    }

    @Override
    public ServiceVO saveOrUpdate(Customer customer) {

        //根据id判断 是新增还是修改操作
        if (customer.getCustomerId() == null){

            //新增操作 先判断是否已经存在此客户
            Customer existCustomer = customerDao.findEXCustomerByName(customer.getCustomerName());

            //如果不为空 则此客户已存在
            if (existCustomer != null){
                return new ServiceVO(ErrorCode.CUSTOMER_EXIST_CODE , ErrorCode.CUSTOMER_EXIST_MESS);
            }

            //否则 执行新增
            customerDao.saveCustomer(customer);
        }else {
            //执行更新操作
            customerDao.updateCustomer(customer);
        }

        //返回
        return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {

        //拿到前端传过来的id 存贷集合里
        List<String> idList = Arrays.asList(ids.split(","));

        customerDao.deleteByIds(idList);

        return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS);
    }
}
