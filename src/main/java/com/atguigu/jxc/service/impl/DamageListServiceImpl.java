package com.atguigu.jxc.service.impl;/*
 * @author: XueYouPeng
 * @time: 23.8.7 下午 7:22
 */

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;

import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Slf4j
public class DamageListServiceImpl implements DamageListService {

    @Autowired
    private DamageListDao damageListDao ;



    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr , HttpSession session) {

        //先拿到用户的id
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        damageList.setUserId(userId);
        //保存第一部分数据
        damageListDao.saveDamageList(damageList);


        //拿到前端传过来的这个商品报损单号
        String damageNumber = damageList.getDamageNumber();
        log.info("商品报损单号:{}" + damageNumber);

        //用商品报损单号查询这条数据
        DamageList damageListValue = damageListDao.findDamage(damageNumber);
        log.info("商品报损数据:{}" + damageListValue);
        //拿到damageListId
        Integer damageListId = damageListValue.getDamageListId();
        log.info("商品报损id:{}" + damageListId);


        //前端传过来的JSON字符串解析成对象
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr , new TypeToken<List<DamageListGoods>>(){}.getType());

        //补全damageListId
        damageListGoodsList.stream().forEach(damageListGoods -> {
            damageListGoods.setDamageListId(damageListId);
        });

        damageListDao.saveDamageListValue(damageListGoodsList);

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    @Override
    public Map<String, Object> list(String sTime, String eTime ,HttpSession session) {

        Map<String, Object> map = new HashMap<>();

        //查询报损单
        List<DamageList> damageListList = damageListDao.findList(sTime , eTime);

        //拿到用户的name
        User user = (User) session.getAttribute("currentUser");
        String trueName = user.getUserName();

        //补全name字段
        damageListList.stream().forEach(damageList -> {
            damageList.setTrueName(trueName);
        });

        //封装数据
        map.put("rows" , damageListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {

        Map<String, Object> map = new HashMap<>();

        //查询报损单商品数据
        List<DamageListGoods> damageListGoodsList = damageListDao.findGoodsList(damageListId);

        //封装数据
        map.put("rows" , damageListGoodsList);

        return map;
    }
}
